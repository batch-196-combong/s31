 /*What is a client?

 A client is an application which creates requests for resources from a server. A client will trigger an action, in the web development context, through a URL and wait for the response of the server. 

 What is a server?

A server is able to host and deliver resources requested by a client. In fact, a single server can handle multiple clients. 

What is a Node.js?

Node.JS is a runtime environment which allows us to create/develop backend/server-side applications with javascript. Because by default, Jacascript was conceptualized solely to he front end. 

Why is Nodejs popular?

Performance - most performing environmen for creating backend application with JS
Familiarity - since Node JS is one of the best built and uses JS as its language, it is very familiar for most developers.
NPM - Node Package Manager is the largest registry for node packages. Packages are bits of programs, methods, functions, codes that greatly help in the development of an application 

*/


// console.log("hello dave");

let http = require("http");
// require() is a built in JS method which allows us to import packages. Packages are pieces of code we can integrate into our application.

// "http" is a default package that coes with node js. It allows us to use methods that let us create servers

// http is a module. Modules are packages we imported.

// Modules are objects that contains codes, functions, methods or data. 

// The http module let us create a server which is able to communicate wth a client through the use of Hyertext Transfer Protocol

// http://localhost:4000

// console.log(http);

http.createServer(function(request,response){

	/*
		createsServer() method is a method from the http module that allows us to handle requests and responses from a client and a server respectively. 

		takes a function argument which is able to receive 2 objects. The request object which contains details of the requests from the clinet. The response object which contains details of the response rom the server. The createServer() method always receives the request object first befor the response. 
	*/

	/*
		response.writeHead() - is a method of the response object. It allows us to add headers to our response. Headers are additional information about our responses. We have 2 arguments in our writeHead() method. The first is the HTTP status code. An HTTp status is just a numerical code to let the client the know about the status of their request. 200, means Ok, 404 means the resource cannot be found. 'Content-type' is one of the most recognizable headers. It simply pertains to the data type of our responses. 

		response.end() it ends our response; It is also able to send a message/data as a string
	*/

	/*
		Serves can actually respond differently with different requests. 

		We start our requests with our URL. A client can start a different requests with a different URL. 

		http://localhost:4000/ is not the same as http://localhost:4000/profile - URL endpoint

		/ = url endpoint
		/profile = url endpoint

		we can differentiate requests by their endpoints, we sould be able to respon differently to different endpoints.

		Information about the url endpoint of the request in in the request object.
		 
	*/

	// console.log(request.url);

	if(request.url === "/"){

	    response.writeHead(200,{'Content-Type':'text/plain'});
	    response.end("Hello from our first server! This is from / endpoint");
	   } else if(request.url === "/profile"){

	   	response.writeHead(200,{'Content-Type':'text/plain'});
	   	response.end("Hi! I am David");
	   }

}).listen(4000);

	/*
		.listen() - allows us to assign a port to our server. This will allow us serve our index.js server in our local machine assigned to port 4000. There are several tasks and processes on our computer that run ondifferent port members. 

		http://localhost:4000/ - server
		localhost: means local machine : 4000 - port assigned to our server.

		usually used ports for web development
		-4000,4040,8000,5000,3000,4200

		console.log() is added to show validation which port number our server is running on. In fact, the more complex our servers become, the more things we can check first before running. 
	*/

console.log("Server is running on localHost:4000!");


